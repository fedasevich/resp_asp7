using WebApplication14.Middlewares;
using WebApplication14.Services.InventoryService;
using WebApplication14.Services.ManufacturerService;
using WebApplication14.Services.PartService;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddTransient<IPartService,PartService>(); // Transient because service is stateless
builder.Services.AddTransient<IManufacturerService, ManufacturerService>(); // Transient because service is stateless
builder.Services.AddTransient<IInventoryService, InventoryService>(); // Transient because service is stateless

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<ExceptionMiddleware>();


app.MapControllers();

app.Run();
