﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApplication14.Models.Dto;
using WebApplication14.Services.InventoryService;
using Microsoft.AspNetCore.Http;
using WebApplication14.Models.Api;
using WebApplication14.Models.Db;
using WebApplication14.Middlewares;

namespace WebApplication14.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InventoryController : ControllerBase
    {
        private readonly IInventoryService _inventoryService;

        public InventoryController(IInventoryService inventoryService)
        {
            _inventoryService = inventoryService;
        }

        /// <summary>
        /// Gets all inventorys.
        /// </summary>
        /// <returns>The inventorys list.</returns>
        /// <response code="200">Returns the inventorys list.</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ApiResponse<Inventory>> GetAll()
        {
            var inventories = await _inventoryService.GetAll();

            return new ApiResponse<Inventory>() { Data = inventories, StatusCode = StatusCodes.Status200OK };
        }

        /// <summary>
        /// Gets a inventory by ID.
        /// </summary>
        /// <param name="id">The ID of the inventory.</param>
        /// <returns>The inventory.</returns>
        /// <response code="200">Returns the inventory.</response>
        /// <response code="404">If the inventory is not found.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ApiResponse<Inventory>> Get(int id)
        {
            var inventory = await _inventoryService.Get(id);

            return new ApiResponse<Inventory>() { Data = new List<Inventory> { inventory }, StatusCode = StatusCodes.Status200OK };
        }

        /// <summary>
        /// Adds a new inventory.
        /// </summary>
        /// <param name="inventoryDto">The inventory to add.</param>
        /// <returns>A status code.</returns>
        /// <response code="201">If the inventory is added successfully.</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ApiResponseEmpty> Add(InventoryDto inventoryDto)
        {
            await _inventoryService.Add(inventoryDto);

            Response.StatusCode = StatusCodes.Status201Created;

            return new ApiResponseEmpty() { StatusCode = StatusCodes.Status201Created };
        }

        /// <summary>
        /// Updates a inventory.
        /// </summary>
        /// <param name="id">The ID of the inventory to update.</param>
        /// <param name="inventoryDto">The updated inventory.</param>
        /// <returns>A status code.</returns>
        /// <response code="200">If the inventory is updated successfully.</response>
        /// <response code="400">If the inventory was not found.</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponseError), StatusCodes.Status400BadRequest)]
        public async Task<ApiResponseEmpty> Update(int id, InventoryUpdateDto inventoryDto)
        {
            await _inventoryService.Update(id, inventoryDto);

            return new ApiResponseEmpty() { StatusCode = StatusCodes.Status200OK };
        }

        /// <summary>
        /// Deletes a inventory.
        /// </summary>
        /// <param name="id">The ID of the inventory to delete.</param>
        /// <returns>A status code.</returns>
        /// <response code="200">If the inventory is deleted successfully.</response>
        /// <response code="400">If the inventory was not found.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponseError), StatusCodes.Status400BadRequest)]

        public async Task<ApiResponseEmpty> Delete(int id)
        {
            await _inventoryService.Delete(id);

            return new ApiResponseEmpty() { StatusCode = StatusCodes.Status200OK };
        }
    }
}