﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApplication14.Models.Dto;
using WebApplication14.Services.PartService;
using Microsoft.AspNetCore.Http;
using WebApplication14.Models.Api;
using WebApplication14.Models.Db;
using WebApplication14.Middlewares;

namespace WebApplication14.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PartController : ControllerBase
    {
        private readonly IPartService _partService;

        public PartController(IPartService partService)
        {
            _partService = partService;
        }

        /// <summary>
        /// Gets all parts.
        /// </summary>
        /// <returns>The parts list.</returns>
        /// <response code="200">Returns the parts list.</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ApiResponse<Part>> GetAll()
        {
            var parts = await _partService.GetAll();

            return new ApiResponse<Part>() { Data = parts, StatusCode = StatusCodes.Status200OK };
        }

        /// <summary>
        /// Gets a part by ID.
        /// </summary>
        /// <param name="id">The ID of the part.</param>
        /// <returns>The part.</returns>
        /// <response code="200">Returns the part.</response>
        /// <response code="404">If the part is not found.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ApiResponse<Part>> Get(int id)
        {
            var part = await _partService.Get(id);

            return new ApiResponse<Part>() { Data = new List<Part> { part }, StatusCode = StatusCodes.Status200OK };
        }

        /// <summary>
        /// Adds a new part.
        /// </summary>
        /// <param name="partDto">The part to add.</param>
        /// <returns>A status code.</returns>
        /// <response code="201">If the part is added successfully.</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ApiResponseEmpty> Add(PartDto partDto)
        {
            await _partService.Add(partDto);

            Response.StatusCode = StatusCodes.Status201Created;

            return new ApiResponseEmpty() { StatusCode = StatusCodes.Status201Created };
        }

        /// <summary>
        /// Updates a part.
        /// </summary>
        /// <param name="id">The ID of the part to update.</param>
        /// <param name="partDto">The updated part.</param>
        /// <returns>A status code.</returns>
        /// <response code="200">If the part is updated successfully.</response>
        /// <response code="400">If the part was not found.</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponseError), StatusCodes.Status400BadRequest)]
        public async Task<ApiResponseEmpty> Update(int id, PartUpdateDto partDto)
        {
            await _partService.Update(id, partDto);

            return new ApiResponseEmpty() { StatusCode = StatusCodes.Status200OK };
        }

        /// <summary>
        /// Deletes a part.
        /// </summary>
        /// <param name="id">The ID of the part to delete.</param>
        /// <returns>A status code.</returns>
        /// <response code="200">If the part is deleted successfully.</response>
        /// <response code="400">If the part was not found.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponseError), StatusCodes.Status400BadRequest)]

        public async Task<ApiResponseEmpty> Delete(int id)
        {
            await _partService.Delete(id);

            return new ApiResponseEmpty() { StatusCode = StatusCodes.Status200OK };
        }
    }
}