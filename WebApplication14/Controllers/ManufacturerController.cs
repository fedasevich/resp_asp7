﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApplication14.Models.Dto;
using WebApplication14.Services.ManufacturerService;
using Microsoft.AspNetCore.Http;
using WebApplication14.Models.Api;
using WebApplication14.Models.Db;
using WebApplication14.Middlewares;

namespace WebApplication14.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ManufacturerController : ControllerBase
    {
        private readonly IManufacturerService _manufacturerService;

        public ManufacturerController(IManufacturerService manufacturerService)
        {
            _manufacturerService = manufacturerService;
        }

        /// <summary>
        /// Gets all manufacturers.
        /// </summary>
        /// <returns>The manufacturers list.</returns>
        /// <response code="200">Returns the manufacturers list.</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ApiResponse<Manufacturer>> GetAll()
        {
            var manufacturers = await _manufacturerService.GetAll();

            return new ApiResponse<Manufacturer>() { Data = manufacturers, StatusCode = StatusCodes.Status200OK };
        }

        /// <summary>
        /// Gets a manufacturer by ID.
        /// </summary>
        /// <param name="id">The ID of the manufacturer.</param>
        /// <returns>The manufacturer.</returns>
        /// <response code="200">Returns the manufacturer.</response>
        /// <response code="404">If the manufacturer is not found.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ApiResponse<Manufacturer>> Get(int id)
        {
            var manufacturer = await _manufacturerService.Get(id);

            return new ApiResponse<Manufacturer>() { Data = new List<Manufacturer> { manufacturer }, StatusCode = StatusCodes.Status200OK };
        }

        /// <summary>
        /// Adds a new manufacturer.
        /// </summary>
        /// <param name="manufacturerDto">The manufacturer to add.</param>
        /// <returns>A status code.</returns>
        /// <response code="201">If the manufacturer is added successfully.</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        public async Task<ApiResponseEmpty> Add(ManufacturerDto manufacturerDto)
        {
            await _manufacturerService.Add(manufacturerDto);

            Response.StatusCode = StatusCodes.Status201Created;

            return new ApiResponseEmpty() { StatusCode = StatusCodes.Status201Created };
        }

        /// <summary>
        /// Updates a manufacturer.
        /// </summary>
        /// <param name="id">The ID of the manufacturer to update.</param>
        /// <param name="manufacturerDto">The updated manufacturer.</param>
        /// <returns>A status code.</returns>
        /// <response code="200">If the manufacturer is updated successfully.</response>
        /// <response code="400">If the manufacturer was not found.</response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponseError), StatusCodes.Status400BadRequest)]
        public async Task<ApiResponseEmpty> Update(int id, ManufacturerUpdateDto manufacturerDto)
        {
            await _manufacturerService.Update(id, manufacturerDto);

            return new ApiResponseEmpty() { StatusCode = StatusCodes.Status200OK };
        }

        /// <summary>
        /// Deletes a manufacturer.
        /// </summary>
        /// <param name="id">The ID of the manufacturer to delete.</param>
        /// <returns>A status code.</returns>
        /// <response code="200">If the manufacturer is deleted successfully.</response>
        /// <response code="400">If the manufacturer was not found.</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ApiResponseError), StatusCodes.Status400BadRequest)]

        public async Task<ApiResponseEmpty> Delete(int id)
        {
            await _manufacturerService.Delete(id);

            return new ApiResponseEmpty() { StatusCode = StatusCodes.Status200OK };
        }
    }
}