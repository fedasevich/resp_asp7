﻿using System.Net;

namespace WebApplication14.Models.Api
{
    public class ApiResponse<T>
    {
        public string Message { get; set; } = string.Empty;
        public int StatusCode { get; set; } = 0;
        public List<T>? Data { get; set; } = new ();
    }

    public class ApiResponseEmpty: ApiResponse<object>
    { }    
}
