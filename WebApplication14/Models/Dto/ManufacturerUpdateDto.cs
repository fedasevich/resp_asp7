﻿using System.ComponentModel.DataAnnotations;
using WebApplication14.Models.Attributes;

namespace WebApplication14.Models.Dto
{
    public class ManufacturerUpdateDto
    {
        [StringLength(255, MinimumLength = 5, ErrorMessage = "{0} length must be between {1} and {2} characters.")]
        public string? Name { get; set; }

        [StringLength(255, MinimumLength = 5, ErrorMessage = "{0} length must be between {1} and {2} characters.")]
        public string? Address { get; set; }

        [NotInFuture]
        public DateTime? EstablishedAt { get; set; }

        [StringLength(255, MinimumLength = 5, ErrorMessage = "{0} length must be between {1} and {2} characters.")]
        public string? Country { get; set; }
    }
}
