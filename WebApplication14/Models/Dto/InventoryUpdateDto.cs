﻿using System.ComponentModel.DataAnnotations;
using WebApplication14.Models.Attributes;

namespace WebApplication14.Models.Dto
{
    public class InventoryUpdateDto
    {
        [Range(1, int.MaxValue, ErrorMessage = "{0} must be greater than 0")]
        public int? PartId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "{0} must be greater than 0")]
        public int? ManufacturerId { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "{0} must be greater than 0")]
        public int? Quantity { get; set; }
    }
}
