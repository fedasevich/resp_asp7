﻿using System.ComponentModel.DataAnnotations;
using WebApplication14.Models.Attributes;

namespace WebApplication14.Models.Dto
{
    public class PartUpdateDto
    {
        [StringLength(255, MinimumLength = 5, ErrorMessage = "{0} length must be between {1} and {2} characters.")]
        public string? Name { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "{0} must be greater than 0")]
        public int? ManufacturerId { get; set; }

        [NotInFuture]
        public DateTime? ManufacturedAt { get; set; }
    }
}
