﻿namespace WebApplication14.Models.Db
{
    public class Inventory
    {
        public int Id { get; set; }
        public int PartId { get; set; }
        public int ManufacturerId { get; set; }
        public int Quantity { get; set; }
    }
}
