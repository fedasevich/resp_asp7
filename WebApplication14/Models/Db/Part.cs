﻿namespace WebApplication14.Models.Db
{
    public class Part
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ManufacturerId { get; set; }

        public DateTime ManufacturedAt { get; set; }
    }
}
