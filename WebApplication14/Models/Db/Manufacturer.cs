﻿namespace WebApplication14.Models.Db
{
    public class Manufacturer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime EstablishedAt { get; set; }
        public string Country { get; set; }
    }
}
