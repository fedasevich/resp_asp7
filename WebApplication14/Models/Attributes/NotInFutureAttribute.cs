﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication14.Models.Attributes
{
    public class NotInFutureAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            if (value is DateTime dateTime && dateTime > DateTime.Now)
            {
                return new ValidationResult("Date cannot be in the future.");
            }

            return ValidationResult.Success;
        }
    }
}
