﻿using Microsoft.AspNetCore.Http;
using System.Net;
using WebApplication14.Models.Api;

namespace WebApplication14.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next, ILogger<ExceptionMiddleware> logger)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = exception is RequestException requestException ? requestException.StatusCode : StatusCodes.Status500InternalServerError;

            Console.WriteLine(exception.Message);
            Console.WriteLine(context.Response.StatusCode);


            var response = new ApiResponseError()
            {
                StatusCode = context.Response.StatusCode,
                Message = exception.Message != null ? exception.Message : "Internal Server Error"
            };

            await context.Response.WriteAsJsonAsync(response);
         
        }
    }

    public class ApiResponseError: ApiResponse<object> { }

    public abstract class RequestException : Exception
    {
        public int StatusCode { get; set; }
        public RequestException(string message, HttpStatusCode statusCode)
        : base(message)
        {
            StatusCode = (int)statusCode;
        }
    }

    public class NotFoundRequestException : RequestException
    {
        public NotFoundRequestException(string message)
        : base(message, HttpStatusCode.NotFound)
        {
        }
    }


    public class BadRequestException : RequestException
    {
        public BadRequestException(string message)
        : base(message, HttpStatusCode.BadRequest)
        {
        }
    }

    public class UnauthorizedException : RequestException
    {
        public UnauthorizedException(string message)
        : base(message, HttpStatusCode.Unauthorized)
        {
        }
    }
}
