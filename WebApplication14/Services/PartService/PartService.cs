﻿using WebApplication14.Middlewares;
using WebApplication14.Models.Db;
using WebApplication14.Models.Dto;
using WebApplication14.Services.ManufacturerService;

namespace WebApplication14.Services.PartService
{
    public class PartService : IPartService
    {
        List<Part> _parts = new List<Part>
{
    new Part { Id = 1, Name = "Engine", ManufacturerId = 1, ManufacturedAt = new DateTime(2020, 1, 1) },
    new Part { Id = 2, Name = "Transmission", ManufacturerId = 2, ManufacturedAt = new DateTime(2020, 2, 1) },
    new Part { Id = 3, Name = "Brakes", ManufacturerId = 3, ManufacturedAt = new DateTime(2020, 3, 1) },
    new Part { Id = 4, Name = "Suspension", ManufacturerId = 4, ManufacturedAt = new DateTime(2020, 4, 1) },
    new Part { Id = 5, Name = "Steering Wheel", ManufacturerId = 5, ManufacturedAt = new DateTime(2020, 5, 1) },
    new Part { Id = 6, Name = "Seats", ManufacturerId = 6, ManufacturedAt = new DateTime(2020, 6, 1) },
    new Part { Id = 7, Name = "Airbags", ManufacturerId = 7, ManufacturedAt = new DateTime(2020, 7, 1) },
    new Part { Id = 8, Name = "Exhaust", ManufacturerId = 8, ManufacturedAt = new DateTime(2020, 8, 1) },
    new Part { Id = 9, Name = "Radiator", ManufacturerId = 9, ManufacturedAt = new DateTime(2020, 9, 1) },
    new Part { Id = 10, Name = "Tyres", ManufacturerId = 10, ManufacturedAt = new DateTime(2020, 10, 1) }
};
        private readonly IManufacturerService _manufacturerService;

        public PartService(IManufacturerService manufacturerService)
        {
            _manufacturerService = manufacturerService;
        }

        public async Task<List<Part>> GetAll()
        {
            await Task.Delay(500);

            return _parts;
        }

        public async Task<Part> Get(int id)
        {
            await Task.Delay(500);

            var part = _parts.FirstOrDefault(p => p.Id == id);

            if (part == null)
            {
                throw new NotFoundRequestException("Part was not found");
            }

            return part;
        }

        public async Task Add(PartDto partDto)
        {
            await Task.Delay(500);

            var manufacturerExists = await _manufacturerService.Get(partDto.ManufacturerId);

            if (manufacturerExists == null)
            {
                throw new BadRequestException("Manufacturer was not found");
            }

            var part = new Part
            {
                Id = !_parts.Any() ? 1 : _parts.Max(item => item.Id) + 1,
                Name = partDto.Name,
                ManufacturedAt = partDto.ManufacturedAt,
                ManufacturerId = partDto.ManufacturerId,
            };

            _parts.Add(part);
        }

        public async Task Update(int id, PartUpdateDto partDto)
        {
            await Task.Delay(500);

            if (partDto.ManufacturerId != null && await _manufacturerService.Get((int)partDto.ManufacturerId) == null)
            {
                throw new BadRequestException("Manufacturer was not found");
            }

            var part = _parts.FirstOrDefault(p => p.Id == id);

            if (part == null)
            {
                throw new BadRequestException("Part was not found");
            }


            var properties = typeof(PartUpdateDto).GetProperties();
            foreach (var property in properties)
            {
                var value = property.GetValue(partDto);
                if (value != null)
                {
                    typeof(Part).GetProperty(property.Name)?.SetValue(part, value);
                }
            }
        }

        public async Task Delete(int id)
        {
            await Task.Delay(500);

            var part = _parts.FirstOrDefault(p => p.Id == id);

            if (part == null)
            {
                throw new BadRequestException("Part was not found");
            }

            _parts.Remove(part);
        }
    }
}
