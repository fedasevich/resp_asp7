﻿using WebApplication14.Models.Db;
using WebApplication14.Models.Dto;


namespace WebApplication14.Services.PartService
{
    public interface IPartService
    {
        public Task<List<Part>> GetAll();
        public Task<Part> Get(int id);
        public Task Add(PartDto partDto);
        public Task Update(int id, PartUpdateDto partDto);
        public Task Delete(int id);
    }
}
