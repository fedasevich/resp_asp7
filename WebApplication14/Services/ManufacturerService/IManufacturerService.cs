﻿using WebApplication14.Models.Db;
using WebApplication14.Models.Dto;


namespace WebApplication14.Services.ManufacturerService
{
    public interface IManufacturerService
    {
        public Task<List<Manufacturer>> GetAll();
        public Task<Manufacturer> Get(int id);
        public Task Add(ManufacturerDto manufacturerDto);
        public Task Update(int id, ManufacturerUpdateDto manufacturerDto);
        public Task Delete(int id);
    }
}
