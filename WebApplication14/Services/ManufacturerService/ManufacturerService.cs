﻿using WebApplication14.Middlewares;
using WebApplication14.Models.Db;
using WebApplication14.Models.Dto;

namespace WebApplication14.Services.ManufacturerService
{
    public class ManufacturerService : IManufacturerService
    {
        List<Manufacturer> _manufacturers = new List<Manufacturer>
{
    new Manufacturer { Id = 1, Name = "Volkswagen", Address = "Berlin, Germany", EstablishedAt = new DateTime(1937, 5, 28), Country = "Germany" },
    new Manufacturer { Id = 2, Name = "Audi", Address = "Ingolstadt, Germany", EstablishedAt = new DateTime(1910, 4, 25), Country = "Germany" },
    new Manufacturer { Id = 3, Name = "Skoda", Address = "Mladá Boleslav, Czech Republic", EstablishedAt = new DateTime(1895, 12, 18), Country = "Czech Republic" },
    new Manufacturer { Id = 4, Name = "Seat", Address = "Martorell, Spain", EstablishedAt = new DateTime(1950, 5, 9), Country = "Spain" },
    new Manufacturer { Id = 5, Name = "Porsche", Address = "Stuttgart, Germany", EstablishedAt = new DateTime(1931, 4, 25), Country = "Germany" },
    new Manufacturer { Id = 6, Name = "Lamborghini", Address = "Sant'Agata Bolognese, Italy", EstablishedAt = new DateTime(1963, 10, 30), Country = "Italy" },
    new Manufacturer { Id = 7, Name = "Bentley", Address = "Crewe, England", EstablishedAt = new DateTime(1919, 1, 18), Country = "United Kingdom" },
    new Manufacturer { Id = 8, Name = "Bugatti", Address = "Molsheim, France", EstablishedAt = new DateTime(1909, 7, 15), Country = "France" },
    new Manufacturer { Id = 9, Name = "Ducati", Address = "Bologna, Italy", EstablishedAt = new DateTime(1926, 7, 4), Country = "Italy" },
    new Manufacturer { Id = 10, Name = "Scania", Address = "Södertälje, Sweden", EstablishedAt = new DateTime(1891, 3, 1), Country = "Sweden" }
};

        public ManufacturerService()
        {
        }

        public async Task<List<Manufacturer>> GetAll()
        {
            await Task.Delay(500);

            return _manufacturers;
        }

        public async Task<Manufacturer> Get(int id)
        {
            await Task.Delay(500);

            var manufacturer = _manufacturers.FirstOrDefault(p => p.Id == id);

            if (manufacturer == null)
            {
                throw new NotFoundRequestException("Manufacturer was not found");
            }

            return manufacturer;
        }

        public async Task Add(ManufacturerDto manufacturerDto)
        {
            await Task.Delay(500);

            var manufacturer = new Manufacturer
            {
                Id = !_manufacturers.Any() ? 1 : _manufacturers.Max(item => item.Id) + 1,
                Name = manufacturerDto.Name,
                Address = manufacturerDto.Address,
                Country = manufacturerDto.Country,
                EstablishedAt = manufacturerDto.EstablishedAt
            };

            _manufacturers.Add(manufacturer);
        }

        public async Task Update(int id, ManufacturerUpdateDto manufacturerDto)
        {
            await Task.Delay(500);

            var manufacturer = _manufacturers.FirstOrDefault(p => p.Id == id);

            if (manufacturer == null)
            {
                throw new BadRequestException("Manufacturer was not found");
            }

            var properties = typeof(ManufacturerUpdateDto).GetProperties();
            foreach (var property in properties)
            {
                var value = property.GetValue(manufacturerDto);
                if (value != null)
                {
                    typeof(Manufacturer).GetProperty(property.Name)?.SetValue(manufacturer, value);
                }
            }
        }

        public async Task Delete(int id)
        {
            await Task.Delay(500);

            var manufacturer = _manufacturers.FirstOrDefault(p => p.Id == id);

            if (manufacturer == null)
            {
                throw new BadRequestException("Manufacturer was not found");
            }

            _manufacturers.Remove(manufacturer);
        }
    }
}
