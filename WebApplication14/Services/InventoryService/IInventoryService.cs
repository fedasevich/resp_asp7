﻿using WebApplication14.Models.Db;
using WebApplication14.Models.Dto;


namespace WebApplication14.Services.InventoryService
{
    public interface IInventoryService
    {
        public Task<List<Inventory>> GetAll();
        public Task<Inventory> Get(int id);
        public Task Add(InventoryDto inventoryDto);
        public Task Update(int id, InventoryUpdateDto inventoryDto);
        public Task Delete(int id);
    }
}
