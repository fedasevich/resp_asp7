﻿using WebApplication14.Middlewares;
using WebApplication14.Models.Db;
using WebApplication14.Models.Dto;
using WebApplication14.Services.ManufacturerService;
using WebApplication14.Services.PartService;

namespace WebApplication14.Services.InventoryService
{
    public class InventoryService : IInventoryService
    {
        List<Inventory> _inventories = new List<Inventory>
{
    new Inventory { Id = 1, PartId = 1, ManufacturerId = 1, Quantity = 100 },
    new Inventory { Id = 2, PartId = 2, ManufacturerId = 2, Quantity = 200 },
    new Inventory { Id = 3, PartId = 3, ManufacturerId = 3, Quantity = 300 },
    new Inventory { Id = 4, PartId = 4, ManufacturerId = 4, Quantity = 400 },
    new Inventory { Id = 5, PartId = 5, ManufacturerId = 5, Quantity = 500 },
    new Inventory { Id = 6, PartId = 6, ManufacturerId = 6, Quantity = 600 },
    new Inventory { Id = 7, PartId = 7, ManufacturerId = 7, Quantity = 700 },
    new Inventory { Id = 8, PartId = 8, ManufacturerId = 8, Quantity = 800 },
    new Inventory { Id = 9, PartId = 9, ManufacturerId = 9, Quantity = 900 },
    new Inventory { Id = 10, PartId = 10, ManufacturerId = 10, Quantity = 1000 }
};
        private readonly IManufacturerService _manufacturerService;
        private readonly IPartService _partService;

        public InventoryService(IManufacturerService manufacturerService, IPartService partService)
        {
            _manufacturerService = manufacturerService;
            _partService = partService;
        }

        public async Task<List<Inventory>> GetAll()
        {
            await Task.Delay(500);

            return _inventories;
        }

        public async Task<Inventory> Get(int id)
        {
            await Task.Delay(500);

            var inventory = _inventories.FirstOrDefault(p => p.Id == id);

            if (inventory == null)
            {
                throw new NotFoundRequestException("Inventory was not found");
            }

            return inventory;
        }

        public async Task Add(InventoryDto inventoryDto)
        {
            await Task.Delay(500);

            var manufacturerExists = await _manufacturerService.Get(inventoryDto.ManufacturerId);

            if (manufacturerExists == null)
            {
                throw new BadRequestException("Manufacturer was not found");
            }

            var partExists = await _partService.Get(inventoryDto.PartId);

            if (partExists == null)
            {
                throw new BadRequestException("Part was not found");
            }

            var inventory = new Inventory
            {
                Id = !_inventories.Any() ? 1 : _inventories.Max(item => item.Id) + 1,
                PartId = inventoryDto.PartId,
                Quantity = inventoryDto.Quantity,
            };

            _inventories.Add(inventory);
        }

        public async Task Update(int id, InventoryUpdateDto inventoryDto)
        {
            await Task.Delay(500);

            if (inventoryDto.ManufacturerId != null && await _manufacturerService.Get((int)inventoryDto.ManufacturerId) == null)
            {
                throw new BadRequestException("Manufacturer was not found");
            }

            if (inventoryDto.PartId != null && await _partService.Get((int)inventoryDto.PartId) == null)
            {
                throw new BadRequestException("Part was not found");
            }

            var inventory = _inventories.FirstOrDefault(p => p.Id == id);

            if (inventory == null)
            {
                throw new BadRequestException("Inventory was not found");
            }


            var properties = typeof(InventoryUpdateDto).GetProperties();
            foreach (var property in properties)
            {
                var value = property.GetValue(inventoryDto);
                if (value != null)
                {
                    typeof(Inventory).GetProperty(property.Name)?.SetValue(inventory, value);
                }
            }
        }

        public async Task Delete(int id)
        {
            await Task.Delay(500);

            var inventory = _inventories.FirstOrDefault(p => p.Id == id);

            if (inventory == null)
            {
                throw new BadRequestException("Inventory was not found");
            }

            _inventories.Remove(inventory);
        }
    }
}
